import type { Middleware } from "koa";
import axios, { AxiosInstance, AxiosResponse } from "axios";
import { Readable } from "stream";
import FormData from "form-data";

declare module "koa" {
  interface ExtendableContext {
    image: Image;
  }
}

export interface Image {
  upload: (
    key: string,
    stream: Readable,
    contentType?: string
  ) => Promise<void>;
  download: (
    key: string,
    params?: Record<string, any>
  ) => Promise<AxiosResponse<Readable>>;
  remove: (key: string[]) => Promise<void>;
}

export type ImageOption = {
  endpoint: string;
};

const uploadImage = (instance: AxiosInstance) => async (
  key: string,
  stream: Readable,
  contentType?: string
): Promise<void> => {
  const formData = new FormData();
  formData.append(key, stream, { contentType });
  await instance.post("/", formData, {
    headers: {
      "Content-Type": "multipart/form-data"
    }
  });
};

const downloadImage = (instance: AxiosInstance) => async (
  key: string,
  params: Record<string, any> = {}
): Promise<AxiosResponse<Readable>> =>
  instance.get<Readable>("/", {
    params: { key, ...params },
    responseType: "stream"
  });
const removeImages = (instance: AxiosInstance) => async (
  key: string[]
): Promise<void> => instance.delete("/", { params: { key } });

const middleware = async (option: ImageOption): Promise<Middleware> => {
  const { endpoint } = option;
  const instance = axios.create({ baseURL: endpoint });
  const upload = uploadImage(instance);
  const download = downloadImage(instance);
  const remove = removeImages(instance);
  return async (ctx, next) => {
    ctx.image = {
      upload,
      download,
      remove
    };
    await next();
  };
};

export default middleware;
