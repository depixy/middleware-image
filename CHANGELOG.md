# [1.1.0](https://gitlab.com/depixy/middleware-image/compare/v1.0.1...v1.1.0) (2020-07-28)


### Features

* add remove ([767ee32](https://gitlab.com/depixy/middleware-image/commit/767ee3256d6544059797db79371f09985c0eebf2))

## [1.0.1](https://gitlab.com/depixy/middleware-image/compare/v1.0.0...v1.0.1) (2020-07-23)


### Bug Fixes

* return promise middleware ([36502d5](https://gitlab.com/depixy/middleware-image/commit/36502d5825e65792d29a84f937890459af9d6029))

# 1.0.0 (2020-07-23)


### Features

* initial commit ([9bc98b1](https://gitlab.com/depixy/middleware-image/commit/9bc98b1d82515e4639b2bce7bab8921bc397da8f))
